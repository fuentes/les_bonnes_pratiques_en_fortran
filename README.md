# les_bonnes_pratiques_en_Fortran

texte (en Markdown) d'une présentation sur les bonnes pratiques de dévelopement en Fortran 90

# Introduction

Le Fortran après de nombreuses évolutions(90, 95, 2003, 2008 et 2018) est devenu un langage permettant
de programmer d'une manière plutôt rigoureuse et concise. Nous souhaitons ici detailler quelques aspects du langage ainsi que
diverses méthodologies permettant de faciliter la maintenance et le développement à long terme d'un programme
en Fortran moderne (x > 2003). Ce langage étant très souvent utilisé par des physiciens, des ingénieurs ou des matheux n'ayant 
pas toujours suivis de formation au génie logiciel, cette présentation a pour but d'inoculer chez ses lecteurs
les premisses d'une prise de conscience que «coder est une activité à part entière» et mérite en conséquence une
attention particulière. Les propos ici ne seront pas forcemment nouveaux par rapport à ceux developpés dans les références, 
mais ils presentent l'avantage d'être écrits en français 😉

# Quelques Généralités

## Le code doit être agréable à lire 

  En conséquence, l'écrivain se doit de faciliter la tâche du lecteur (qui sera sans doute aussi l'écrivain la plupart du temps),
  en rendant son code clair, concis et auto-commenté. Il peut notamment : 
  - ajouter des commentaires, lorsque que le nom des variables ou des fonctions n'est pas suffisament explicite.
  - surcharger des operations pour rendre son code aussi mathématique que possible (cf patron Abstract calculus)
  - s'efforcer de factoriser du code grâce aux fonctions, sous-routines et types derivés

## Favoriser la modularité

  Un code évolutif est un code qui minimise les interdépendances entre les principales parties du code. Le fortran, grâce
  au _modules_ et aux _types_ permet d'isoler et d'encapsuler au sein d'une même unité textuelle, les données et les
  fonctions qui agissent sur ces structures de données. Au programmeur alors de choisir astucieusement les meilleurs
  interfaces de programmations (API), afin de minimiser les dépendances et de maximiser la flexibilité d'usage de ces
  fonctions. Dans un même esprit, il est bon de minimiser les effets de bord (par exemple avec _pure_ ) et de supprimer
  les variables globales, qui sont des abominations du point de vue de la modularité.


## Le **COPIER-COLLER est une mauvaise pratique**

  Il peut arriver lorsque qu'on a un article à soumettre de devoir travailler dans l'urgence et de recopier un pan entier
  de code en le modifiant quelque peu pour traiter un nouveau cas (du code rapide et crade 😑). Ce genre de rustine peut
  s'averer ponctuellement efficace, mais il est clair qu'a la longue elle **NUIT GRAVEMENT** à la maintenabilité du code.
  Les codes en Fortran, comportent de nombreuses erreurs et plus il y a de ligne de code, plus on perd de temps a chercher
  une aiguille dans une botte de foin. Le programmeur doit donc garder à l'esprit cette recherche de parcimonie lors 
  de l'écriture de code. Plus de temps de développement pour moins de ligne de code, c'est beaucoup moins de temps de 
  perdu en déverminage ulterieurement.

## Des exemples et des tests

 Certains pratiques comme le dévelopement guidé par les tests (TDD) peuvent sembler un peu trop radicales, à des programmeurs
 venant du monde mathématique ; si les numériciens comprennent bien l'utilité de tester un code une fois qu'il est grosso modo
 developpé, ils ne comprennent pas toujours la nécéssité d'éxecuter automatiquement une suite de tests, chaque fois que le code
 est modifié. Écrire une bonne serie de tests automatiques, c'est s'assurer d'avoir une bonne raquette avec le moins de trous
 possibles, lorsque que l'on est à la recherche de bug. l'autre avantage des tests est qu'ils permettent de fournir des
 exemples d'utilisation de tout ou d'une partie du code (modules, fonctions, types). Nous verrons ultérieurement comment
 mettre en place une suite de tests à l'aide du systeme de compilation CMake.

# Documentation

 La documenation (à l'instar des tests), fait elle aussi figure de parent pauvre lors des procssesus de développement. Pourtant
 une bonne documentation est la garantie d'une meilleure utilisation des fonctions de l'interfaces d'un code par
 des développeurs externes. A part à vouloir être l'unique developpeur de son code, le programmeur doit s'efforcer de documenter
 correctement les fonctions et types qu'ils developpent. Les cadriciels Doxygen ou Ford peuvent être utilisés dans le but
 de rediger de la documentation développeur.



## Du code!
 On va d'abord voir à l'aide de fonctionnalités de Fortran 90 et au travers d'exemple, comment factoriser du code


### des tableaux "a la python ou à la matlab"
Soit le code suivant : 
```fortran
v(1, 1) = coor(node(3), 1) - coor(node(2), 1)
v(1, 2) = coor(node(3), 2) - coor(node(2), 2)
v(2, 1) = coor(node(1), 1) - coor(node(3), 1)
v(2, 2) = coor(node(1), 2) - coor(node(3), 2)
v(3, 1) = coor(node(2), 1) - coor(node(1), 1)
v(3, 2) = coor(node(2), 2) - coor(node(1), 2)
```
Un programmeur avisé ecrira moitié moins de ligne avec
```fortran
v(1,:) = coor(node(3), :) - coor(node(2), :)
v(2,:) = coor(node(1), :) - coor(node(3), :)
v(3,:) = coor(node(2), :) - coor(node(1), :)
```
Mais on pourra gagner, en perdant peut-etre en lisibilité en écrivant
```
v([1,2,3],:) = coor(node([3,1,2]), :) - coor(node([2,3,1]), :)
```


# Références 

- Modern Fortran Explained, Reid, Metcalf & Cohen
- Scientific Software Developement, Rouson et al.
- Modern Fortran, Curcić
- Parallel Programming with Co-Arrays, Numrich
